<?php if(false): ?>
    <style type="text/css">
<?php endif ?>


    #Index{
        margin: auto;
        margin-left: 170px;
        width: 75%;
    }

    .menu-container {
        position: fixed;
        left: 0;
    }
    
    *{
        font-family: arial;
        padding: 0;
        margin: 0;
    }

    div::selection, img::selection, td::selection, li::selection{
        background: <?php echo($mainColor) ?>;
    }

    .t-left{text-align: left;}

    .t-center{text-align: center;}

    .t-right{text-align: right;}

    .t-upper{text-transform: uppercase;}

    .t-s{font-size: 10px;}

    .t-m{font-size: 12px;}

    .t-b{font-size: 16px;}

    .t-under{text-decoration:underline;}

    #detector{
        height: 100%;
        width: 0px;
        position: absolute;
    }

    .main{
        display: block;
    }

    .full{
        width: 100%;
        height: 100%;
        position: absolute;
        z-index: 5;
    }

    .center{
        position: relative;
        margin-left: auto;
        margin-right: auto;
    }

    .sep{
        max-height: 1px;
        height: 1px;
        border-bottom: dotted 2px #307054;
    }

    .tip{
        color: <?php echo($textColor) ?>;
        padding-right: 5px;
    }

    .des{
        text-shadow: 1px 1px #010101;
        text-transform: uppercase;
        font-size: 10px;
        color: #6b8fc9;
    }

    .red{
        color: #b06d68;
    }

    .grey{
        background-color: <?php echo($lightColor) ?>;
    }

    .pointer{
        cursor: pointer;
    }

    .stripes{
        height: 14px;
        background: url('<?php echo($imgDir) ?>stripes.png');
    }

    .line{
        border-top: solid 1px <?php echo($lightColor) ?>;
    }

body {
    font-family: arial;
    font-size: 13px;
    margin: 0px;
    padding: 0px;
    background: url('<?php echo($imgDir) ?>page-bg.jpg');
}
    /*----------COLORS----------*/
.main-color {
    color: <?php echo($mainColor) ?>;
}

.light-color{
    color: <?php echo($lightColor) ?>;
}

.dark-color{
    color: <?php echo($darkColor) ?>;
}

    /*----------TEXT----------*/
.small-text {
    font-size: 11px;
    text-transform: uppercase;
    font-weight: bold;
}

.mid-text {
    font-size: 18px;
    text-transform: uppercase;
    font-weight: bold;
    line-height: 15px;
}

.big-text {
    font-size: 23px;
    text-transform: uppercase;
    font-weight: bolder;
    letter-spacing: -2px;
    line-height: 22px;
    text-align: right;
    padding-left: 15px;
}

.lowercase-text {
    text-transform: lowercase;
}

.italic {
    font-style: italic;
}

.text-shadow {
    text-shadow: <?php echo($lightColor) ?> 0px 0px 1px;
}


#page table:first-child {
    margin-left: 170px;
}

.container {
    background: url('<?php echo($imgDir) ?>page-bg.jpg');
}

.space {
    background-color: #d1d1d1;
    opacity: 0.2;
    height: 80px;
}

.yline {
    background: url('<?php echo($imgDir) ?>yline.jpg');
    width: 2px;
}

.xline {
    background: url('<?php echo($imgDir) ?>xline.jpg');
    height: 2px;
}

.bap{
    min-width: 2px;
    background: url('<?php echo($imgDir) ?>yline.jpg');
}

