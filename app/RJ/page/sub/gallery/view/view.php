<?php

    $numb = $GLOBALS['data']['data']['album'];
    $cat = $GLOBALS['data']['data']['cat'];
    
    
    $tree = $_SESSION['data']['tree'][$cat];
    $album = $tree['set'][$numb];
    
    $data = $GLOBALS['data'];
    
    $url = $_SESSION['data'][$album['id']]['url'];
?>
<td align="center" class="container" >
    <div class="Gallery">
        <div>
            <table cellpadding="0" cellspacing="5">
                <tr>
                    <td class="big-text dark-color text-shadow"><?php echo($album['title']) ?></td>
                    <td class="small-text light-color"><p>Landscape</p></td>
                    <td class="yline"></td>
                    <td class="small-text main-color"><p><?php echo($album['date']) ?></p></td>
                </tr>
                <tr>
                    <td class="xline" colspan="4"></td>
                </tr>
                <tr>
                    <td class="italic text-shadow" colspan="4">
                        <p><?php echo($album['description']) ?> truxt aprasimu daugiau</p>
                    </td>
                </tr>
                <tr>
                    <td class="gallery-arrow" colspan="4"></td>
                </tr>
                <tr>
                    <td class="big-photos" colspan="4" align="center">
                        <?php foreach($url as $val): ?>
                        <img src="<?php echo($val) ?>">
                        <?php endforeach; ?>
                    </td>
                </tr>
            </table>
        <div>
    </div>
</td>
