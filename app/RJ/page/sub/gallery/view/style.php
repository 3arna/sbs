<?php if(false): ?>
    <style type="text/css">
<?php endif ?>

.Gallery {
    padding: 50px;
}

.Gallery table tr:first-child td {
    text-align: left;
}

.Gallery table tr:first-child td +td {
    text-align: right;
    vertical-align: bottom;
}

.Gallery table tr:first-child td +td +td +td {
    text-align: left;
    vertical-align: bottom;
}

.gallery-arrow {
    background-image:url('<?php echo($imgDir) ?>gallery-arrow.png');
    height: 24px;
    background-color: <?php echo($mainColor) ?>;
}

.big-photos{
    padding-top: 20px;
}

.big-photos img{
    background: #ffffff;
    padding: 2px;
    max-width: 870px;
    min-width: 870px;
    border: #C5C5C5 solid 1px;
    outline: #ffffff solid thin;
    margin: 5px;
}

p{
   margin-bottom: 2px;
}
