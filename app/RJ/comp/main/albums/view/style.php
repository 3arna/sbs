<?php if(false): ?>
    <style type="text/css">
<?php endif ?>

#Albums{
    width: 946px;
    height: 800px;
}

.bubble {
    background: url('<?php echo($imgDir) ?>bubble.png') no-repeat center;
    width: 36px;
    height: 36px;
    background-color: <?php echo($mainColor) ?>;
}

.Album {
    width: 270px;
    float: left;
    padding-left: 45px;
}

.Album .yline {
    min-height: 36px;
}

.album-bg{
    background: url('<?php echo($imgDir) ?>yline.jpg') repeat-y;
    background-position: 179px 0px;
}

    /*.Album tr:first-child +tr +tr td div {
        height: 36px;
    }*/

.bubble div{
    font-weight: bold;
    font-size: 16px;
    color: #ffffff;
    padding-top: 9px;
}

.Text-container {
    background:url('<?php echo($imgDir) ?>yline.jpg') repeat-y center;
    z-index: 1;
}

.small-photos {
    color: #C0C0C0;
    background-color: #ffffff;
    outline: #ffffff solid thin;
    border: #c5c5c5 solid 1px;
    -moz-box-shadow:    0px 0px 25px 1px #B6B6B6;
    -webkit-box-shadow: 0px 0px 25px 1px #B6B6B6;
    box-shadow:         0px 0px 25px 1px #B6B6B6;
    position: relative;
    margin: 15px;
    margin-bottom: -15px;

}

.small-photos img {
    margin-top: -70px;
    left: 0px;
    width: 233px;
    max-height: 200px;
    border: #ffffff solid 2px;
}

.small-photos img:first-child {
    margin:0px;
    width: 233px;
    border: #ffffff solid 2px;
}

.Text {
    background:url('<?php echo($imgDir) ?>text-bg.png') no-repeat center bottom #fbfbfb;
    outline: #ffffff solid thin;
    border: #c5c5c5 solid 1px;
    border-bottom: none;
    text-align: right;
    padding: 8px;
    padding-top: 20px;
}

.Text tr:first-child +tr +tr +tr td {
    text-align: left;
}

.description{
    overflow: hidden;
    width: 250px;
}

.bot-yline{
    min-height: 20px;
    background: url('<?php echo($imgDir) ?>yline.jpg');
    width: 2px;
}

.albums-arrow {
    background:url('<?php echo($imgDir) ?>albums-arrow.png');
    width: 191px;
    height: 24px;
    background-color: <?php echo($mainColor) ?>;
}

.half-bubble {
    background:url('<?php echo($imgDir) ?>half-bubble.png');
    width: 36px;
    height: 49px;
    position: relative;
}

.bubble-arrow-top {
    background:url('<?php echo($imgDir) ?>bubble-arrow-top.png');
    width: 25px;
    height: 15px;
    position: absolute;
    margin-top: 15px;
    margin-left: 10px;
}

.bubble-arrow-bot {
    background:url('<?php echo($imgDir) ?>bubble-arrow-bot.png');
    width: 25px;
    height: 15px;
    position: absolute;
    margin-top: 20px;
    margin-left: 10px;
}

.half-bubble div:hover {
    cursor: pointer;
}

.bubble-arrows {
    width: 25px;
    float: right;
    margin-right: -41px;
}
