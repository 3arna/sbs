<?php if(false): ?>
<script>
<?php endif ?>


    function indexPhotos_loadAndSwitch(interval, fadeTime){
    
        
        var indexPhotos = load('c', 'indexPhotos', 'index');
        $('#indexPhotos').html(indexPhotos);
        var index = load('g', 'index');
        var index = jQuery.parseJSON(index);
        
        var temp=new Array();
        var i=0;
        var l=0;
        for(i in index.url){
            temp[l] = index.url[i];
            l++;
        }
        l=l-1;
        
        var photos = $("#striped-header .photo");
        var imgs = $("#striped-header .photo img");
        
        loop(imgs, photos, temp, l, interval, fadeTime);
        
        setInterval(function() {
            
            loop(imgs, photos, temp, l, interval, fadeTime);
            
        }, interval*3);
        
    };
    
    function loop(imgs, photos, temp, l, interval, fadeTime){
       
        var n=0;
        
        setTimeout(function() {
            temp.splice(0,0,temp[l]);
            temp.pop();
            changeImg(imgs, temp, photos, n, fadeTime);
            n++;
        }, interval);
        
        setTimeout(function() {
            changeImg(imgs, temp, photos, n, fadeTime);
            n++;
        }, interval*2);
        
        setTimeout(function() {
            changeImg(imgs, temp, photos, n, fadeTime);
            n++;
        }, interval*3);
    }
    
    function changeImg(imgs, temp, photos, n, fadeTime){
        var src = imgs.eq(n).attr('src');
        photos.eq(n).css('background-image', 'url('+src+')');
        imgs.eq(n).fadeOut(0, function(){
            imgs.eq(n).attr('src', temp[n]).fadeIn(fadeTime)
        });
    }