<?php if (false): ?>
<style type="text/css">
    <?php endif ?>

    #striped-header{
        background-image: url("<?php echo $imgDir ?>stripes.png");
        padding-top: 7px;
        padding-bottom: 7px;
        overflow: hidden;
        width: 761px;
    }

    #striped-header .photo{
        position: relative;
        margin-left: 7px;
        border: 1px solid #ffffff;
        width: 242px;
        float: left;
        cursor: pointer;
        background-size:242px 200px;
        background-repeat:no-repeat;
    }

    .photo img{
        display:block;
        width: 242px;
        height: 200px;
        box-shadow:inset 0 0 10px #000000;
    }

    .shadow{
        display:block;
        position:relative;
    }

    .shadow::before{
        display:block;
        content:'';
        position:absolute;
        width:100%;
        height:100%;
        -moz-box-shadow:inset 0px 0px 3px 3px rgba(0,0,0,0.2);
        -webkit-box-shadow:inset 0px 0px 3px 3px rgba(0,0,0,0.2);
        box-shadow:inset 0px 0px 3px 3px rgba(0,0,0,0.2);
    }

    .brush-gray-l{
        height: 29px;
        width: 242px;
        background-image: url("<?php echo $imgDir ?>brush-gray-l.png");
        position: absolute;
        top: 0;
    }

    .brush-gray-l p{
        text-align: right;
        text-transform: uppercase;
        font-weight: bold;
        color: #ffffff;
        font-family: Arial;
        font-size: 14px;
        margin-top: 4px;
        margin-right: 5px;
    }

    .brush-gray-r{
        height: 29px;
        width: 242px;
        background-image: url("<?php echo $imgDir ?>brush-gray-r.png");
        position: absolute;
        top: 0;
    }

    .brush-gray-r p{
        text-align: left;
        text-transform: uppercase;
        font-weight: bold;
        color: #ffffff;
        font-family: Arial;
        font-size: 14px;
        margin-top: 4px;
        margin-left: 5px;
    }

    .brush-orange{
        position: absolute;
        bottom: 0;
        height: 29px;
        width: 242px;
        background-image: url("<?php echo $imgDir ?>brush-orange.png");
    }

    .brush-orange p{
        text-align: center;
        text-transform: uppercase;
        font-weight: bold;
        color: #ffffff;
        font-family: Arial;
        font-size: 14px;
        margin-top: 9px;
    }