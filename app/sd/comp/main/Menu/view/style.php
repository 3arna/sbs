<?php if (false): ?>
    <style type="text/css">
<?php endif ?>

#Menu{
    width: 761px;
}

#constructer-top{
    height: 31px;
    width: 139px;
    background-image: url("<?php echo $imgDir ?>constructer-top.png");
}

#constructer-bot{
    height: 30px;
    width: 139px;
    background-image: url("<?php echo $imgDir ?>constructer-bot.png");
}

#arrow-icon-l{
    height: 12px;
    width: 13px;
    background-image: url("<?php echo $imgDir ?>arrow-icon-l.png");
    float: right;
    margin-right: 10px;
}

.arrow-icon-r{
    height: 12px;
    width: 13px;
    background-image: url("<?php echo $imgDir ?>arrow-icon-r.png");
    float: left;
    margin-left: 10px;
}

.arrow-border-l{
    width: 100px;
    border-right: 1px solid <?php echo $lineDark ?>;
    border-bottom: 1px solid <?php echo $lineDark ?>;
}

.arrow-border-r{
    width: 100px;
    border-left: 1px solid <?php echo $lineDark ?>;
    border-bottom: 1px solid <?php echo $lineDark ?>;
}

.side-border-l{
    border-left: 1px solid <?php echo $lineLight ?>;
    border-right: 1px solid <?php echo $lineLight ?>;
    border-bottom: 1px solid <?php echo $lineDark ?>;
    padding-right: 10px;
    padding-left: 10px;
}

.side-border-d{
    border-left: 1px solid <?php echo $lineDark ?>;
    border-right: 1px solid <?php echo $lineDark ?>;
    border-bottom: 1px solid <?php echo $lineDark ?>;
    padding-right: 10px;
    padding-left: 10px;
}

.border-top{
    border-top: 1px solid <?php echo $lineLight ?>;
}

#constructer-border{
    width: 139px;
    border-left: 1px solid <?php echo $lineLight ?>;
    border-right: 1px solid <?php echo $lineLight ?>;
}

#border-transp{
    border-left: 1px solid transparent;
    border-right: 1px solid transparent;
}

.text-s{
    font-size: 10px;
    font-family: Arial;
    color: <?php echo $mainColor ?>;
    text-transform: uppercase;
    margin-bottom: -5px;
}

.text-b{
    font-size: 16px;
    font-family: Arial;
    color: <?php echo $darkColor ?>;
    text-transform: uppercase;
}

.align-l{
    text-align: left;
}

.align-r{
    text-align: right;
}
    
.active{
    border-bottom: 2px solid <?php echo $mainColor ?>;
}

.button{
    cursor: pointer;
}