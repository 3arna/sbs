<?php
?>

<div>
    <table id="Menu" cellpadding='0' cellspacing='0'>
        <tr>
            <td class="h-lineLight" colspan="7"></td>
        </tr>
        <tr>
            <td class="h-lineDark" colspan="7"></td>
        </tr>
        <tr>
            <td class="arrow-border-l">
                <div id="arrow-icon-l"></div>
            </td>
            <td class="side-border-l align-l button active" id="about">
                <div>
                    <div class="text-s">trumpai</div>
                    <div class="text-b">apie mus</div>
                </div>
            </td>
            <td class="side-border-d align-l button" id="works">
                <div>
                    <div class="text-s">darbu</div>
                    <div class="text-b">galerija</div>
                </div>
            </td>
            <td id="constructer-border">
                <div id="constructer-top"></div>
            </td>
            <td class="side-border-d align-r button" id="furnitures">
                <div>
                    <div class="text-s">baldų</div>
                    <div class="text-b">gamyba</div>
                </div>
            </td>
            <td class="side-border-l align-r button" id="contacts">
                <div>
                    <div class="text-s">užduok</div>
                    <div class="text-b">klausimą</div>
                </div>
            </td>
            <td class="arrow-border-r">
                <div class="arrow-icon-r"></div>
            </td>
        </tr>
        <tr>
            <td class="border-top"></td>
            <td class="border-top"></td>
            <td class="border-top"></td>
            <td id="border-transp">
                <div id="constructer-bot"></div>
            </td>
            <td class="border-top"></td>
            <td class="border-top"></td>
            <td class="border-top"></td>
        </tr>
    </table>
</div>