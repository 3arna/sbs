<?php if(false): ?>
<script>
<?php endif ?>

    function Menu_changeSubPage(name){
        var subpage;
        if(name != 'works' && name != 'furnitures'){
            subpage = load('s', name);
        }
        else
            subpage = load('s', 'gallery', name);

        $('#subpage').html(subpage);

        if(name == 'works' || name == 'furnitures'){
            var photo = $(".mini-photo img").eq(0);
            $("#photo img").attr('src', photo.attr('src')).attr('alt', 0);
            $("#comment p").text(photo.attr('alt'));
        }
    };