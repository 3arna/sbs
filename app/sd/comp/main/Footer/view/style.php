<?php if (false): ?>
<style type="text/css">
    <?php endif ?>

    #Footer table{
        width: 761px;
        border-right: 1px solid <?php echo $mainColor ?>;
    }

    #striped-footer{
        background-image: url("<?php echo $imgDir ?>stripes.png");
        width: 761px;
        height: 7px;
    }

    #Footer .h-lineLight{
        width: 761px;
    }

    #Footer .h-lineDark{
        width: 761px;
    }

    .side-borders{
        height: 31px;
        border-right: 1px solid <?php echo $lineDark ?>;
        border-left: 1px solid <?php echo $lineLight ?>;
    }

    .left-border{
        border-right: 1px solid <?php echo $lineDark ?>;
    }

    #sbs{
        width: 31px;
        box-shadow:inset 0 0 10px <?php echo $lineDark ?>;
        border: 1px solid #FFFFFF;
        background-color: #FFFFFF;
    }

    #sbs:hover{
        box-shadow:inset 0 0 10px #808180;
    }

    #sbs-art img{
        width: 31px;
        height: 31px;
    }

    #mini-logo{
        padding-left: 10px;
        padding-top: 4px;
    }

    #mini-logo img{
        width: 65px;
        height: 25px;
        float: left;
    }

    #slogan{
        font-size: 10px;
        font-family: Arial;
        text-transform: uppercase;
        color: <?php echo $darkColor ?>;
        float: left;
        padding: 8px;
    }

    .support img{
        width: 70px;
        height: 25px;
    }

    .support{
        text-align: center;
        width: 100px;
        cursor: pointer;
    }