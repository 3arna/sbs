<?php
?>

<div id="Footer">
    <div id="striped-footer"></div>
    <div class="h-lineLight"></div>
    <div class="h-lineDark"></div>
    <table cellpadding='0' cellspacing='0'>
        <tr>
            <td class="left-border"></td>
            <td class="side-borders"></td>
            <td class="side-borders support">
                <a href="http://st-technika.lt/">
                    <img src="http://st-technika.lt/image/data/sisteminiai/logo.png">
                </a>
            </td>
            <td class="side-borders" width='220'>
                <div id="mini-logo">
                    <img src="<?php echo $imgDir ?>logo.png">
                </div>
                <div id="slogan">"mes dirbame jums"</div>
            </td>
            <td id="sbs">
                <div id="sbs-art">
                    <a href="http://www.sbs.eu5.org">
                        <img src="<?php echo $imgDir ?>sbs-art.png">
                    </a>
                </div>
            </td>
        </tr>
    </table>
    <div class="h-lineDark"></div>
    <div class="h-lineLight"></div>
</div>