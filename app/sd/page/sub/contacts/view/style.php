<?php if (false): ?>
    <style type="text/css">
<?php endif ?>

    #ContactUs{
        text-align: center;
        margin: auto;
        margin-bottom: 41px;
        margin-top: 10px;
    }

    #ContactUs textarea{
        width: 350px;
        height: 200px;
        font-family: arial;
        font-size: 12px;
        text-transform: uppercase;
        text-align: center;
        color: <?php echo $mainColor ?>;
        border: 1px solid <?php echo $lineLight ?>;
        outline: <?php echo $lineDark ?> solid 1px;
        background-color: transparent;
        resize: none;
        margin-top: 10px;
    }

    .data{
        width: 250px;
        height: 40px;
        font-family: arial;
        font-size: 12px;
        text-transform: uppercase;
        text-align: center;
        color: <?php echo $darkColor ?>;
        border: 1px solid <?php echo $lineDark ?>;
        outline: <?php echo $lineLight ?> solid 1px;
        background-color: transparent;
        margin-top: 10px;
    }

    .submit{
        width: 250px;
        height: 40px;
        font-family: arial;
        font-size: 12px;
        text-transform: uppercase;
        text-align: center;
        color: #FFFFFF;
        border: 1px solid <?php echo $lineLight ?>;
        outline: <?php echo $lineDark ?> solid 1px;
        background-color: <?php echo $mainColor ?>;
        margin-top: 10px;
        cursor: pointer;
    }

    .submit:hover{
        color: <?php echo $mainColor ?>;
        border: 1px solid <?php echo $lineLight ?>;
        outline: <?php echo $lineDark ?> solid 1px;
        background-color: <?php echo $lightColor ?>;
        cursor: pointer;
    }

    .h-lineDark{
        height: 1px;
        background-color: <?php echo $lineDark ?>;
    }

    .h-lineLight{
        height: 1px;
        background-color: <?php echo $lineLight ?>;
    }

    .success{
        width: 350px;
        text-align: center;
        font-size: 9px;
        color: #ffffff;
        text-transform: uppercase;
        background-color: #a3cdaf;
        padding-top: 5px;
        padding-bottom: 5px;
        border: 1px solid <?php echo $lineDark ?>;
        display: none;
    }

    .error{
        width: 550px;
        text-align: center;
        font-size: 9px;
        color: #ffffff;
        text-transform: uppercase;
        background-color: #ffa191;
        padding-top: 5px;
        padding-bottom: 5px;
        border: 1px solid <?php echo $lineDark ?>;
        display: none;
    }
    
    .contacts-text{
        margin-right: 30px;
    }
    
    .contacts-title{
        text-transform: uppercase;
        font-family: Arial;
        font-size: 14px;
        color: <?php echo $mainColor ?>;
    }
    
    .contacts-text .text{
        font-style: italic;
        font-size: 11px;
        font-family: Arial;
        color: <?php echo $darkColor ?>;
    }
    
    