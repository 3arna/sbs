<?php
$imgDir = 'img/';
$darkColor = '#444444';
$lightColor = '#cccccc';
$textColor = '#7a7a7a';
$mainColor = '#f87c0e';
$lineDark = '#c6b38b';
$lineLight = '#ffffff';
$c3 = '#99ffce';
?>
<form action="" method="post">
    <table id="ContactUs" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <div class="contacts-text">
                    <table cellspacing="0" cellpadding="0">
                        <tr> 
                            <td height="28">
                                <div class="contacts-title">kontaktai</div>
                            </td>
                            <td align=" right">
                                <div class="text">Remigijus, Klaipėda</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="mail-icon"></div>
                            </td>
                            <td>
                                <div class="text">remis.statyba@gmail.com</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <div class="phone-icon"></div>
                            </td>
                            <td>
                                <div class="text">+37069968596</div>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td>
                <textarea name="msg" placeholder="">jūsų klausimai ar pageidavimai</textarea>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <div class="success">
                    Jūsų žinutė sėkmingai išsiųsta. Atsakymą gausite į nurodytus kontaktinius duomenis.
                </div>
                <div class="error">
                    žinutė neišsiųsta. Užpildykite abu laukelius, patikrinkite ar nurodėte teisingus kontaktinius duomenis.
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <input type="text" class="data" name="data" placeholder="jūsų kontaktiniai duomenys">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <input type="submit" name="submit" class="submit" value="siųsti">
            </td>
        </tr>
    </table>
</form>