<?php if (false): ?>
<style type="text/css">
<?php endif ?>

#Gallery{
    padding-top: 10px;
    padding-bottom: 20px;
    margin: 0 auto;
    display: inline-block;
}

#Gallery table{
    text-align: center;
}

#arrow-l{
    width: 35px;
    height: 34px;
    background-image: url("<?php echo $imgDir ?>arrow-l.png");
    cursor: pointer;
    float: right;
    margin-right: 30px;
}

#arrow-r{
    width: 35px;
    height: 34px;
    background-image: url("<?php echo $imgDir ?>arrow-r.png");
    cursor: pointer;
    float: left;
    margin-left: 30px;
}

.mini-photo{
    border: 1px solid <?php echo $lineLight ?>;
    outline: <?php echo $lineDark ?> solid 1px;
    margin-left: 5px;
    margin-right: 5px;
    margin-bottom: 10px;
    float: left;
    width: 100px;
    height: 70px;
}

.mini-photo img{
    width: 100px;
    height: 70px;
    cursor: pointer;
}

#photo{
    margin: auto;
    margin-top: 10px;
    border: 1px solid <?php echo $lineLight ?>;
    outline: <?php echo $lineDark ?> solid 1px;
}

#photo img{
    cursor: pointer;
    width: 759px;
}

#mini{
    width: 561px;
    overflow: hidden;
    text-align: center;
}

#mini-container{
    padding-top: 5px;
    height: 80px;
    overflow: hidden;
}

#comment{
    font-size: 12px;
    font-family: Arial;
    font-style: italic;
    color: <?php echo $darkColor ?>;
    margin: auto;
    margin-top: 3px;
    margin-bottom: 3px;
}

.h-lineDark{
    height: 1px;
    background-color: <?php echo $lineDark ?>;
}

.h-lineLight{
    height: 1px;
    background-color: <?php echo $lineLight ?>;
}

#Gallery .arrow-icon-r{
    margin-left: 350px;
    margin-right: 5px;
}