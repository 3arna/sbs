<?php
$imgDir = 'img/';
$darkColor = '#444444';
$lightColor = '#cccccc';
$textColor = '#7a7a7a';
$mainColor = '#f87c0e';
$lineDark = '#c6b38b';
$lineLight = '#ffffff';
$c3 = '#99ffce';


$data = $GLOBALS['data'];
$subpage = $GLOBALS['data']['data'][0];
get($data['met']['flickr'], $data['req'][$subpage], $data['con'][0], $subpage);

?>

<div id="Gallery">
    <table cellspacing="0" cellpadding="0">
        <?php if($subpage=='furnitures'): ?>
        <tr>
            <td colspan="3">
                <div class="arrow-icon-r"></div>
                <div class="title">baldų gamyba</div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div class="text-content">
                    <p>
                        Pagal individualius klientų užsakymus projektuojame, gaminame, atvežame ir surenkame virtuvės, prieškambario, svetainės, miegamojo, vaikų kambario bei kitos paskirties baldus. Baldų gamybai naudojame laminuotas medžio drožlių plokštes (LMDP), dažytas medžio dulkių plokštes (MDF). Baldams naudojame Hettich Blum ir kitų gerai žinomų firmų furnitūrą. Taip pat siūlome ekonomiškesnius sprendimus su kokybiškais tačiau pigesniais priedais.
                    </p>
                </div>
            </td>
        </tr>
        <tr>
            <td class="h-lineDark" colspan="5"></td>
        </tr>
        <tr>
            <td class="h-lineLight" colspan="5"></td>
        </tr>
        <?php endif; ?>
        <tr>
            <td width="100px">
                <div class="arrow" id="arrow-l"></div>
            </td>
            <td align="center">
                <div id="mini-container">
                    <div id="mini">
                        <?php foreach($_SESSION['data'][$subpage]['url'] as $key => $val): ?>
                            <div class="mini-photo">
                                <a>
                                    <img src="<?php echo $val ?>" alt="<?php echo $_SESSION['data'][$subpage]['title'][$key] ?>">
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </td>
            <td width="100px">
                <div class="arrow" id="arrow-r"></div>
            </td>
        </tr>
        <tr>
            <td class="h-lineDark" colspan="5"></td>
        </tr>
        <tr>
            <td class="h-lineLight" colspan="5"></td>
        </tr>
        <tr>
            <td colspan="5">
                <div id="comment"><p>Nuotraukos komentaras...</p></div>
            </td>
        </tr>
        <tr>
            <td class="h-lineLight" colspan="5"></td>
        </tr>
        <tr>
            <td class="h-lineDark" colspan="5"></td>
        </tr>
        <tr>
            <td colspan="5">
                <div id="photo">
                    <img src="<?php echo $imgDir ?>photo2.jpg">
                </div>
            </td>
        </tr>
    </table>
</div>
