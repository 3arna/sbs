<?php
$imgDir = 'img/';
$darkColor = '#444444';
$lightColor = '#cccccc';
$textColor = '#7a7a7a';
$mainColor = '#f87c0e';
$lineDark = '#c6b38b';
$lineLight = '#ffffff';
$c3 = '#99ffce';
?>

<div id="AboutUs">
    <table cellspacing="0" cellpadding="0">
    <tr>
        <td colspan="2">
            <div class="arrow-icon-r"></div>
            <div class="title">apie mus</div>
        </td>
    </tr>
        <tr>
            <td colspan="2">
                <div class="text-content">
                    <p>
                        Modernus požiūris į atliekamus darbus, patyręs kolektyvas lėmė sėkmę ir leido įsitvirtinti tarp kitų statybos rinkos dalyvių. Mes nuolat domimės skirtingų statybų sričių naujovėmis - susipažįstame su šiuolaikiškomis medžiagomis bei moderniais sprendimais ir stengiamės juos įdiegti. Atsakingi ir patyrę meistrai teikia namo vidaus, išorės įrengimo ir apdailos darbus. Pagrindinis mūsų tikslas - pilnai patenkinti mūsų klientų poreikius ir išpildyti, net sudėtingiausius pageidavimus. Mes siūlome už konkurencingą kainą ypač aukštą kokybę ir greitai atliktus darbus. Mes visuomet pasirengę bendradarbiauti, patarti ir suteikti reikiamą informaciją ar išsamią konsultaciją jus dominančiais klausimais. Patarsime, interjero dizaino klausimais ir kokios medžiagos tinkamiausios konkrečiam Jūsų darbui.</br>
                    </p>
                </div>
            </td>
        </tr>
        <tr>
            <td class="lines">
                <div class="h-lineDark"></div>
                <div class="h-lineLight"></div>
            </td>
            <td>
                <div id="ww-icon">
                    <img src="<?php echo $imgDir ?>ww-icon.png">
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="text-content">
                    <p>
                        Mes dirbame tam, kad įgyvendintumėme visas, net ir pačias drąsiausias Jūsų idėjas! Mūsų komandoje dirba žmonės, turintys didelės praktikos ir kompetencijos šioje srityje. Darbuotojai, kurių požiūris į darbą atsakingas, sudaro stiprų kolektyvą. Suprantame, kad patikėdami savo rūpesčius ir svajones mums, tikitės pozityvaus, efektyvaus, sąžiningo, grįsto pasitikėjimu ir tarpusavio supratimu bendradarbiavimo. Garantuojame savo klientui, kad darbas bus atliktas laiku, kokybiškai ir tenkins finansinius jo lūkesčius. Mes visada siekiame ilgalaikio bendradarbiavimo, mums svarbus kiekvienas klientas. Už puikų kokybės ir kainos santykį atliekame, tiek mažos, tiek dideles apimties darbus, todėl Jums nereikės ieškotis meistrų kiekvienam darbui atskirai. Tokiu būdu sutaupysite šiuo metu labai branginamą laiką ir žinoma pinigų, nes kuo daugiau darbų vienoje vietoje mums, tuo didesnes nuolaidas galime taikyti Jums. Susisiekite su mumis, atvyksime apžiūrėti Jūsų objekto, padėsime rasti optimaliausius sprendimus, ir juos įgyvendinti. Taip pat galite atsiųsti savo pasiūlymus el. paštu.
                    </p>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="h-lineDark"></div>
                <div class="h-lineLight"></div>
            </td>
            <td>
                <div id="ww-icon">
                    <img src="<?php echo $imgDir ?>wwd-icon.png">
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="text-content">
                    <p>
                        • Apdailos ir remonto darbai. Apdaila nuo A iki Z...</br>
                        • Interjero kūrimas</br>
                        • Gyvenamųjų namų statyba</br>
                        • Fasadų apdaila ir remontas</br>
                        • Stogų įrengimas</br>
                        • Baldų gamyba bei projektavimas
                    </p>
                </div>
            </td>
        </tr>
    </table>
</div>