<?php if (false): ?>
<style type="text/css">
<?php endif ?>

#AboutUs{
    width: 761px;
    margin: 0 auto;
    display: inline-block;
    padding-top: 10px;
    padding-bottom: 10px;
}

.text-content{
    font-family: Arial;
    font-size: 12px;
    color: <?php echo $darkColor ?>;
    margin-top: 10px;
    margin-bottom: 5px;
    margin-bottom: 5px;
}

.lines .h-lineLight{
    width: 610px;
    height: 1px;
    background-color: <?php echo $lineLight ?>;
}

.lines .h-lineDark{
    width: 610px;
    height: 1px;
    background-color: <?php echo $lineDark ?>;
}

#ww-icon{
    padding-left: 30px;
    padding-right: 70px;
}

#ww-icon img{
    height: 34px;
    width: 40px;
}

.title {
    text-transform: uppercase;
    font-family: Arial;
    font-size: 14px;
    color: <?php echo $mainColor ?>;
    float: left;
}

#AboutUs .arrow-icon-r{
    margin-left: 350px;
    margin-right: 5px;
}

