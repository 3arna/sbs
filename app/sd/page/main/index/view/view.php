<?php
?>

<div id="mainContainer">
    <table id="mainTable" cellpadding='0' cellspacing='0'>
        <tr>
            <td class="v-lineDark"></td>
            <td class="v-lineLight"></td>
            <td class="v-space"></td>
            <td class="line-stripes"></td>
            <td class="v-space"></td>
            <td class="v-lineLight"></td>
            <td class="v-lineDark"></td>
            <td id="content">
                <table cellpadding='0' cellspacing='0'>
                    <tr>
                        <td class="h-lineLight" colspan='5'></td>
                    </tr>
                    <tr>
                        <td class="h-lineDark" colspan='5'></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="email">remis.statyba@gmail.com</div>
                        </td>
                        <td>
                            <div class="mail-icon"></div>
                        </td>
                        <td width='220px'>
                            <div class="name">remigijus</div>
                        </td>
                        <td>
                            <div class="phone-icon"></div>
                        </td>
                        <td>
                            <div class="phone">+37069968596</div>
                        </td>
                    </tr>
                    <tr>
                        <td class="h-lineDark" colspan='5'></td>
                    </tr>
                    <tr>
                        <td class="h-lineLight" colspan='5'></td>
                    </tr>
                    <tr>
                        <td colspan='5'>
                            <div id="shadow-top"></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='5'>
                            <div id="logo"></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <div id="logo-slogan">Darbą atliekame kruopščiai ir nepriekaištingai pagal jūsų pageidavimus</div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='5'>
                            <div id="shadow-bot"></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="h-lineLight" colspan='5'></td>
                    </tr>
                    <tr>
                        <td class="h-lineDark" colspan='5'></td>
                    </tr>
                    <tr>
                        <td class="h-space" colspan="5"></td>
                    </tr>
                    <tr>
                        <td colspan="5" id="indexPhotos">
                        </td>
                    </tr>
                    <tr>
                        <td class="h-space" colspan="5"></td>
                    </tr>

                    <tr>
                        <td colspan="5">
                            <?php view::show("Menu") ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="center">
                            <div id="subpage"></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <?php view::show("Footer") ?>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="v-lineDark"></td>
            <td class="v-lineLight"></td>
            <td class="v-space"></td>
            <td class="line-stripes"></td>
            <td class="v-space"></td>
            <td class="v-lineLight"></td>
            <td class="v-lineDark"></td>
        </tr>
    </table>
</div>
