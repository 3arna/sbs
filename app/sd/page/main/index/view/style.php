<?php if (false): ?>
    <style type="text/css">
<?php endif ?>

body{
    margin: 0;
    padding: 0;
    background-image: url("<?php echo $imgDir ?>bg-o.jpg");
    text-shadow: 0px 0px 1px <?php echo $darkColor ?>;
}

#mainContainer{
    margin: auto;
    width: 950px;
}

#mainTable{
    height: 100%;
}

.v-lineLight{
    width: 1px;
    background-color: <?php echo $lineLight ?>;
}

.v-lineDark{
    width: 1px;
    background-color: <?php echo $lineDark ?>;
}

.line-stripes{
    width: 7px;
    background-image: url("<?php echo $imgDir ?>stripes.png");
}

.v-space{
    width: 2px;
}

#content{
    width: 750px;
    background-image: url("<?php echo $imgDir ?>bg-i.jpg");
    padding-left: 75px;
    padding-right: 75px;
    vertical-align: top;
}

.h-lineLight{
    height: 1px;
    background-color: <?php echo $lineLight ?>;
}

.h-lineDark{
    height: 1px;
    background-color: <?php echo $lineDark ?>;
}

.email{
    text-align: right;
    font-size: 12px;
    font-family: Arial;
    font-style: italic;
    color: <?php echo $darkColor ?>;
}

.mail-icon{
    height: 27px;
    width: 29px;
    background-image: url("<?php echo $imgDir ?>mail-icon.png");
    margin: auto;
}

.name{
    text-align: center;
    font-size: 12px;
    font-family: Arial;
    font-style: italic;
    color: <?php echo $darkColor ?>;
    text-transform: uppercase;
}

.phone-icon{
    height: 28px;
    width: 29px;
    background-image: url("<?php echo $imgDir ?>phone-icon.png");
    margin: auto;
}

.phone{
    text-align: left;
    font-size: 12px;
    font-family: Arial;
    font-style: italic;
    color: <?php echo $darkColor ?>;
    margin-right: 20px;
}

#shadow-top{
    height: 28px;
    width: 761px;
    background-image: url("<?php echo $imgDir ?>shadow-top.png");
    margin: auto;
}

#logo{
    height: 63px;
    width: 156px;
    background-image: url("<?php echo $imgDir ?>logo.png");
    margin: auto;
}

#logo-slogan{
    text-align: center;
    font-size: 10px;
    font-family: Arial;
    color: <?php echo $darkColor ?>;
    margin: auto;
}

#shadow-bot{
    height: 28px;
    width: 761px;
    background-image: url("<?php echo $imgDir ?>shadow-bot.png");
    margin: auto;
}

.h-space{
    height: 5px;
}
