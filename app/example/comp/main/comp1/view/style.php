<?php
?>

#comp1{
    background-color:#000000;
    width:50px;
    height:150px;
}

#comp1 table{
    border:solid 2px #ffffff;
    padding:5px;
}

#comp1 h1 {
    color:#E01B5D;
}

#comp1 h2 {
    color:#1BD6E0;
}

#comp1 p {
    color:#4A4D49;
    font-size:15px;
    font-style:italic;
}