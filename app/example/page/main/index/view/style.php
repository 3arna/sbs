<?php
?>

#header{
    width: 500px;
    background-color: <?php echo($darkColor)?>;
    height:100px;
    margin:auto;
    text-align:center;
}

#header p {
    color:#ffffff;
    font-size:20px;
    font-family:Arial;
    text-transform:uppercase;
    margin:0;
    line-height:100px;
}

#mc_holder_comp1{
    float:left;
}

#sc_holder_elem{
    float:left;
}