<?php


    //constants
	define('HOST', '//'.$_SERVER['SERVER_NAME']);
    define('DS', '/');                                  //directory separator
    define('VM', '?');                                //value mark in url
    define('VS', '&');                                  //value separator
    define('AM', '~');                                  //action marker in url
    define('PM', '#');                                  //page marker in url
    //object markers eg. [data:1,g:azas];[j:3]
    define('OS', ';');                                  //object separator
    define('SOM', '[');                                 //starting object marker
    define('EOM', ']');                                 //ending object marker
    define('EQ', ':');                                  //object value equivalent

    define('ROOT', dirname(dirname(__FILE__)));

    //pub link
    define('URL', HOST.substr($_SERVER['SCRIPT_NAME'], 0, strpos($_SERVER['SCRIPT_NAME'], '/pub/')+5));
    
    //lib
    define('DIR_LIB', ROOT.DS.'lib'.DS);
    define('DIR_LIB_EXT', DIR_LIB.'ext'.DS);

    //pub directories
    define('DIR_PUB', ROOT.DS.'pub'.DS);

    //fw directories
    define('DIR_APP', ROOT.DS.'app'.DS);
    define('DIR_COMMON', DIR_APP.'~common'.DS);
    define('DIR_LOGS', ROOT.DS.'logs'.DS);
    define('DIR_SYS', ROOT.DS.'sys'.DS);



