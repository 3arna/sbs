<?php if(false): ?>
    <script>
<?php endif; ?>

/*---------------------------load inner page-------------------------*/
    function load(type, name, data){
        var url;
        var dataType = 'html';
        if(data)
           url = "?"+type+"="+name+"&d="+data;
        else
           url = "?"+type+"="+name;
           
        if(dataType == 'g')
            dataType = 'json';

        var loadData;
        $.ajax({
            url: url,
            async: false,
            dataType: dataType
        }).done(function(data){
            loadData = data;
        });
        return loadData;
    }

    function postData(){
        $("form").submit( function (e) {
            var form = $(this);
            e.preventDefault();
            $.ajax({
                url: form.attr('action'),
                type: "POST",
                dataType: "text",
                data: form.serialize()
            }).done(function(data){
                $('.success, .error').hide();
                if(data == 'success')
                    $('.success').show();
                else
                    $('.error').show();
            });

        });
    };

/*---------------------------absolute-center-------------------------------*/
    function absoluteCenter(element){
        element.css({'left':(windowProps.w/2)-(element.width()/2), 'top':(windowProps.h/2)-(element.height()/2)});
    }

    function fixTop(scrollTop){

    }

/*---------------------------fix elements to left side----------------------*/
    function fixLeft(scrolledLeft, fixedElements){
        for(var i in fixedElements){
            fixedElements[i].dom.css({'left':(fixedElements[i].left)-scrolledLeft});
        };
    };

/*---------------------------collect fixed elements left position-----------*/
    function collectFixedElementsLeft(elements){
        var data = new Array();
        for(var index in elements){
            data[index] = new Array();
            data[index]['dom'] = $(elements[index]);
            data[index]['left'] = $(elements[index]).position().left;
        };
        return data;
    };
/*---------------------------redirect to external page---------------*/
    function redirect(page){
        if(pages.enabled)
            window.location = pages[page];
    }
/*---------------------------request for data with request-----------*/
    function request(req, src, format){
        format = format || "json";
        src = src || "get";
        var reqData;
        $.ajax({
            url: "?r="+req+"&s="+src,
            async: false,
            dataType: format
        }).done(function(data){
            reqData = data;
                if(displayData)
                    console.log(data);
        });

        if(!reqData && !displayData)
            redirect('error');
        else
            return reqData;
    }
/*---------------------------getElement------------------------------*/
    function getElement(){

    }

/*---------------------------load inner page-------------------------*/

    function dump(data){
        console.log(data);
    }