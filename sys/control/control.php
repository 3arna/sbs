<?php

    if($_POST){
        if($_POST['msg'] && $_POST['data']){
            mail($GLOBALS['data']['email'], $_POST['data'], $_POST['msg']);
            echo('success');
        }
        else
            echo('error');
    }
    else{
        ini_set('session.gc_probability', 1);
        ini_set('session.gc_maxlifetime', 6000);
        session_start();
        /*session_unset();*/
        if(isset($_GET['g'])){
            $valid = (preg_match("/^[0-9a-zA-Z\:\;]+$/", $_GET['g'])==1)?true:false;
            if($valid)
                load('g', $_GET['g']);
        }
        else{
            if(isset($_GET['d'])){
                $valid = (preg_match("/^[0-9a-zA-Z\:\;]+$/", $_GET['d'])==1)?true:false;
                if($valid)
                    $GLOBALS['data']['data']=convert($_GET['d'], ':', ';');
            }
    
            if(!isset($_GET['m']) && !isset($_GET['s']) && !isset($_GET['c'])){
                load('m');
            }
            else{
                if(isset($_GET['s']))
                    load('s', $_GET['s']);
                else{
                    if(isset($_GET['c']))
                        load('c', $_GET['c']);
                }
            }
        }
    }
?>