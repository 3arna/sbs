<?php


    $GLOBALS['APP']['URL']['MAIN'] = URL.$GLOBALS['APP']['NAME'].DS;
    
    //public app urls
    $GLOBALS['APP']['URL']['CSS'] = $GLOBALS['APP']['URL']['MAIN'].'css'.DS;
    $GLOBALS['APP']['URL']['JS'] = $GLOBALS['APP']['URL']['MAIN'].'js'.DS;
    $GLOBALS['APP']['URL']['IMG'] = $GLOBALS['APP']['URL']['MAIN'].'img'.DS;
    $GLOBALS['APP']['URL']['UPLOAD'] = URL.'upload'.DS.$GLOBALS['APP']['NAME'].DS;

    //main dir and default dir and cfg
    $GLOBALS['APP']['DIR']['MAIN'] = DIR_APP.$GLOBALS['APP']['NAME'].DS;
    $GLOBALS['APP']['DIR']['DEF'] = $GLOBALS['APP']['DIR']['MAIN'].'app.def'.DS;
    $GLOBALS['APP']['DIR']['CONTROL'] = $GLOBALS['APP']['DIR']['DEF'].'control'.DS;
    $GLOBALS['APP']['DIR']['VIEW'] = $GLOBALS['APP']['DIR']['DEF'].'view'.DS;
    $GLOBALS['APP']['DIR']['SCRIPT'] = $GLOBALS['APP']['DIR']['DEF'].'script'.DS;
    //page and part
    $GLOBALS['APP']['DIR']['PAGE'] = $GLOBALS['APP']['DIR']['MAIN'].'page'.DS;
    $GLOBALS['APP']['DIR']['DEFPAGE'] = $GLOBALS['APP']['DIR']['PAGE'].'def'.DS;
    $GLOBALS['APP']['DIR']['SUBPAGE'] = $GLOBALS['APP']['DIR']['PAGE'].'sub'.DS;
    $GLOBALS['APP']['DIR']['MAINPAGE'] = $GLOBALS['APP']['DIR']['PAGE'].'main'.DS;
    
    $GLOBALS['APP']['DIR']['COMP'] = $GLOBALS['APP']['DIR']['MAIN'].'comp'.DS;
    $GLOBALS['APP']['DIR']['DEFCOMP'] = $GLOBALS['APP']['DIR']['COMP'].'def'.DS;
    $GLOBALS['APP']['DIR']['SUBCOMP'] = $GLOBALS['APP']['DIR']['COMP'].'sub'.DS;
    $GLOBALS['APP']['DIR']['MAINCOMP'] = $GLOBALS['APP']['DIR']['COMP'].'main'.DS;



