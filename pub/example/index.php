<?php
$data = array(
        'req' => array(
            'album' => 'flickr...method:getPhotos;',
            'tree' => 'flickr...method:getTree;user_id:96622359@N06,c_name:I',
            'part' => ',date:1,count:5,size:q'
        ),
        'con' => array(
            'aHR0cDovL25vbmVlZHN5c3RlbS5ldTAxLmF3cy5hZi5jbS9wdWIv'
        ),
        'pages' => array(
            'pup', 'gallery', 'contacts'
        ),
        'elements' => array(
            'album'
        ),
        'email' => 'zarniavicius@gmail.com',
        $_SERVER['SERVER_ADDR'],
        $_SERVER['REMOTE_ADDR']
    );

    function load($type, $name='index'){
        
        $valid = (preg_match("/^[0-9a-zA-Z]+$/", $name)==1)?true:false;
        
        /*$valid = 1;*/
        if($valid){
        
            switch($type){
                case 'm':
                    $file = $name.'.html';
                    $folder = 'main';
                    break;
                
                case 's':
                    $file = $name.'.php';
                    $folder = 'sub';
                    break;
                
                case 'c':
                    $file = $name.'.php';
                    $folder = 'comp';
                    break;
            }
                
            $path = $folder.'/'.$file;
            
            if(file_exists($path))
                include_once($path);
        }
    }
    
    
    if(!isset($_GET['m']) && !isset($_GET['s']) && !isset($_GET['c'])){
        load('m');
    }
    else{
        if(isset($_GET['s']))
            load('s', $_GET['s']);
        else{ 
            if(isset($_GET['c']))
                load('c', $_GET['c']);
        }
    }
?>