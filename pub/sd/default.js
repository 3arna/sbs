
    function Menu_changeSubPage(name){
        var subpage;
        if(name != 'works' && name != 'furnitures'){
            subpage = load('s', name);
        }
        else
            subpage = load('s', 'gallery', name);

        $('#subpage').html(subpage);

        if(name == 'works' || name == 'furnitures'){
            var photo = $(".mini-photo img").eq(0);
            $("#photo img").attr('src', photo.attr('src')).attr('alt', 0);
            $("#comment p").text(photo.attr('alt'));
        }
    };
    window.onload = function (){
    
    Menu_changeSubPage('about');
    indexPhotos_loadAndSwitch(3000, 1000);
    
    };
    
    $(document).ready(function() {
    


    $('.button').live('click', function(){
        var id = $(this).attr('id');
        Menu_changeSubPage(id);
        $('.button').removeClass('active');
        $(this).addClass('active');
    });

    $(".mini-photo img").live('click', function(){
        var numb = $(".mini-photo img").index(this);
        $("#photo img").attr('src', $(this).attr('src')).attr('alt', numb);
        $("#comment p").text($(this).attr('alt'));
    });

    $(".arrow").live('click', function(){
        var cont = $("#mini");
        var marg = parseInt(cont.css('margin-top'));
        var i = $(".arrow").index(this);
        if(i==0 && marg<0)
            cont.css('margin-top', marg+82);
        if(i==1 && -marg < cont.height()-82){
            cont.css('margin-top', marg-82);
        }
    });

    $("#ContactUs").live('click', function(){
        postData();
    });
    
    $("#photo img").live('click', function(){
        var numb = parseInt($(this).attr('alt')) + 1;
        var src = $(".mini-photo img").eq(numb).attr('src');
        $("#comment p").text($(".mini-photo img").eq(numb).attr('alt'));
        $(this).attr('src', src).attr('alt', numb);
    });
    });
    
/*---------------------------load inner page-------------------------*/
    function load(type, name, data){
        var url;
        var dataType = 'html';
        if(data)
           url = "?"+type+"="+name+"&d="+data;
        else
           url = "?"+type+"="+name;
           
        if(dataType == 'g')
            dataType = 'json';

        var loadData;
        $.ajax({
            url: url,
            async: false,
            dataType: dataType
        }).done(function(data){
            loadData = data;
        });
        return loadData;
    }

    function postData(){
        $("form").submit( function (e) {
            var form = $(this);
            e.preventDefault();
            $.ajax({
                url: form.attr('action'),
                type: "POST",
                dataType: "text",
                data: form.serialize()
            }).done(function(data){
                $('.success, .error').hide();
                if(data == 'success')
                    $('.success').show();
                else
                    $('.error').show();
            });

        });
    };

/*---------------------------absolute-center-------------------------------*/
    function absoluteCenter(element){
        element.css({'left':(windowProps.w/2)-(element.width()/2), 'top':(windowProps.h/2)-(element.height()/2)});
    }

    function fixTop(scrollTop){

    }

/*---------------------------fix elements to left side----------------------*/
    function fixLeft(scrolledLeft, fixedElements){
        for(var i in fixedElements){
            fixedElements[i].dom.css({'left':(fixedElements[i].left)-scrolledLeft});
        };
    };

/*---------------------------collect fixed elements left position-----------*/
    function collectFixedElementsLeft(elements){
        var data = new Array();
        for(var index in elements){
            data[index] = new Array();
            data[index]['dom'] = $(elements[index]);
            data[index]['left'] = $(elements[index]).position().left;
        };
        return data;
    };
/*---------------------------redirect to external page---------------*/
    function redirect(page){
        if(pages.enabled)
            window.location = pages[page];
    }
/*---------------------------request for data with request-----------*/
    function request(req, src, format){
        format = format || "json";
        src = src || "get";
        var reqData;
        $.ajax({
            url: "?r="+req+"&s="+src,
            async: false,
            dataType: format
        }).done(function(data){
            reqData = data;
                if(displayData)
                    console.log(data);
        });

        if(!reqData && !displayData)
            redirect('error');
        else
            return reqData;
    }
/*---------------------------getElement------------------------------*/
    function getElement(){

    }

/*---------------------------load inner page-------------------------*/

    function dump(data){
        console.log(data);
    }

    function indexPhotos_loadAndSwitch(interval, fadeTime){
    
        
        var indexPhotos = load('c', 'indexPhotos', 'index');
        $('#indexPhotos').html(indexPhotos);
        var index = load('g', 'index');
        var index = jQuery.parseJSON(index);
        
        var temp=new Array();
        var i=0;
        var l=0;
        for(i in index.url){
            temp[l] = index.url[i];
            l++;
        }
        l=l-1;
        
        var photos = $("#striped-header .photo");
        var imgs = $("#striped-header .photo img");
        
        loop(imgs, photos, temp, l, interval, fadeTime);
        
        setInterval(function() {
            
            loop(imgs, photos, temp, l, interval, fadeTime);
            
        }, interval*3);
        
    };
    
    function loop(imgs, photos, temp, l, interval, fadeTime){
       
        var n=0;
        
        setTimeout(function() {
            temp.splice(0,0,temp[l]);
            temp.pop();
            changeImg(imgs, temp, photos, n, fadeTime);
            n++;
        }, interval);
        
        setTimeout(function() {
            changeImg(imgs, temp, photos, n, fadeTime);
            n++;
        }, interval*2);
        
        setTimeout(function() {
            changeImg(imgs, temp, photos, n, fadeTime);
            n++;
        }, interval*3);
    }
    
    function changeImg(imgs, temp, photos, n, fadeTime){
        var src = imgs.eq(n).attr('src');
        photos.eq(n).css('background-image', 'url('+src+')');
        imgs.eq(n).fadeOut(0, function(){
            imgs.eq(n).attr('src', temp[n]).fadeIn(fadeTime)
        });
    }