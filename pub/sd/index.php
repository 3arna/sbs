<?php

    function load($type, $name='index'){
        $valid = (preg_match("/^[0-9a-zA-Z]+$/", $name)==1)?true:false;
        
        if($valid){
            $method = 'file';
            switch($type){
                case 'm':
                    $file = $name.'.html';
                    $folder = 'main';
                    break;
                case 's':
                    $file = $name.'.php';
                    $folder = 'sub';
                    break;
                case 'c':
                    $file = $name.'.php';
                    $folder = 'comp';
                    break;
                case 'g':
                    $method = 'data';
                    $data = json_encode($_SESSION['data'][$name]);
                    break;
                    
            }
            if($method != 'data'){
                $path = $folder.'/'.$file;
                if(file_exists($path))
                    include_once($path);
            }
            else
                print($data);
            
        }
    }
    function get($method, $req, $con, $name){
        
        $url = base64_decode($con).$method.$req;
        /*echo($req);*/
        
        if(!isset($_SESSION['data'][$name])){
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_CONNECTTIMEOUT => 10,
                CURLOPT_TIMEOUT => 20
            ));

            $data = curl_exec($curl);
            $_SESSION['data'][$name] = json_decode($data, true);
            curl_close($curl);
        }

    }
    function convert($string, $eq, $dsep){
        $tempData = explode($dsep, $string);
        foreach($tempData as $key => $val){
            $data = explode($eq, $val);
            if(count($data)==2){
                $tempData[$data[0]] = $data[1];
                unset($tempData[$key]);
            }
        }
        return $tempData;
    }
    function dump($data, $deep=0){
        $tab = '   ';
        $space = '';
        $strCol = '#cc0000';
        $keyCol = '#13489E';
        $arowCol = '#888a85';
        
        for($i=0; $i<$deep; $i++){
            $space = $space.$tab;
        }
        
        if($deep==0)
            echo('<pre>-------------------------------------------------------------</br>');
        else
            echo('<pre>');
        if(isset($data)){
            if(is_array($data)){
                $keys = array_keys($data);
                foreach($keys as $k){
                    if(is_array($data[$k])){
                        echo($space.'<b>(array)</b> <font color="'.$keyCol.'">'.$k.'</font> => <b>{</b>');
                        dump($data[$k], $deep+1);
                        echo($space.'<b>}</b></br>');
                    }
                    else
                        echo($space.'<small>(string)</small> <font color="'.$keyCol.'">['.$k.']</font> => <font color="'.$strCol.'">'.htmlspecialchars($data[$k]).'</font></br>');
                    
                }
                
            }   
            else
                echo('(string) => <font color="'.$strCol.'">'.htmlspecialchars($data).'</font></br>');
        }
        else
            echo('empty</br>');
        
        if($deep==0)
            echo('-------------------------------------------------------------</pre>');
        else
            echo('</pre>');
    }
    
    $GLOBALS['data'] = array(
        'met' => array(
            'flickr' => 'flickr...method:getPhotos;'
        ),
        'req' => array(
            'works' => 'photoset_id:72157635754324875',
            'furnitures' => 'photoset_id:72157635754483035',
            'index' => 'photoset_id:72157635754201286,size:m'
        ),
        'con' => array(
            'aHR0cDovL25vbmVlZHN5c3RlbS5ldTAxLmF3cy5hZi5jbS9wdWIv'
        ),
        'email' => 'remis.statyba@gmail.com'
    );
    

    if($_POST){
        if($_POST['msg'] && $_POST['data']){
            mail($GLOBALS['data']['email'], $_POST['data'], $_POST['msg']);
            echo('success');
        }
        else
            echo('error');
    }
    else{
        ini_set('session.gc_probability', 1);
        ini_set('session.gc_maxlifetime', 6000);
        session_start();
        /*session_unset();*/
        if(isset($_GET['g'])){
            $valid = (preg_match("/^[0-9a-zA-Z\:\;]+$/", $_GET['g'])==1)?true:false;
            if($valid)
                load('g', $_GET['g']);
        }
        else{
            if(isset($_GET['d'])){
                $valid = (preg_match("/^[0-9a-zA-Z\:\;]+$/", $_GET['d'])==1)?true:false;
                if($valid)
                    $GLOBALS['data']['data']=convert($_GET['d'], ':', ';');
            }
    
            if(!isset($_GET['m']) && !isset($_GET['s']) && !isset($_GET['c'])){
                load('m');
            }
            else{
                if(isset($_GET['s']))
                    load('s', $_GET['s']);
                else{
                    if(isset($_GET['c']))
                        load('c', $_GET['c']);
                }
            }
        }
    }
?>