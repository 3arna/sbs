<?php

/** view main class */
class view{

    //------------------DEFAULT APP OPTIONS
    public $defaultViewFileName = 'index';
    public $defaultViewCssName = 'style';
    public $defaultViewJsName = 'script';
    public $cssData = array();

    public $defaultViewFolder = 'view';

    public $rewriteCss = false;
    public $loadCss = true;
    //-------------------------------------

    public $data = array();                 //get data['translation'] and save translated data from page class
    public $staticData = array(             //get data['ready'] and save from page class
        'data' => array()                   //get data['page'] and save from page class
    );
    public $componentsView = array();       //saves component views | start from lowest lvl from page class
    public $componentsCss = array();

    public function __construct($view, $pageType){
        $this->pageType = $pageType;
        if($view == 'default'){
            $this->viewName = 'default';
            $this->cssData['imgDir'] = 'default';
        }
        else{
            $this->viewName = $view;
            $this->cssData['imgDir'] = $view;
        }
    }

    /**
     * about    - load view by his name
     * @param string $view_name     name of the view folder
     * @param array $data           passed data to view
     * @param bool $display         show or return view
     */

    public function load($component_view_name, $display=false, $data=false){

        //OPTIMIZE THIS SEARCH FOLDER shit....
        /*if($display===true || $display==='page'){
            $folderName = folder::searchFolder($view_name, DIR_APP_VIEW_PAGE);
            if($folderName)
                $path = DIR_APP_VIEW_PAGE.$folderName.DS;
        }
        else{
            if(folder::isSubstring($view_name, '~', false)){
                $display=true;
                $path = DIR_APP_VIEW.$view_name.DS;
            }
            else{
                $folderName = folder::searchFolder($view_name, DIR_APP_VIEW_COMPONENT);
                $path = DIR_APP_VIEW_COMPONENT.$folderName.DS;
            }
        }*/

        $isDefault = false;

        if($display===true || $display==='page'){
            $folderName = folder::searchFolder($component_view_name, $GLOBALS['APP']['DIR']['PAGE']);
            if($folderName)
                $path = $GLOBALS['APP']['DIR']['PAGE'].$folderName.DS.$this->defaultViewFolder.DS;
        }
        else{
            if(folder::isSubstring($component_view_name, '~', false)){
                $isDefault = true;
                $path = $GLOBALS['APP']['DIR']['VIEW'].$this->defaultViewFolder.DS.$component_view_name.DS;
            }
            else{
                $folderName = folder::searchFolder($component_view_name, $GLOBALS['APP']['DIR']['COMPONENT']);
                if($folderName)
                    $path = $GLOBALS['APP']['DIR']['COMPONENT'].$folderName.DS.$this->defaultViewFolder.DS;
            }
        }

        $app = $GLOBALS['APP'];

        ob_start();

        //extract data if is set
        @require($GLOBALS['APP']['DIR']['VIEW'].'view.cfg.php');
        if($app)
            extract($app, EXTR_SKIP);

        $viewLoaded = false;

        if($this->viewName){

            if($isDefault){
                $newPath = substr($path, 0, strpos($path, $component_view_name));
                $fullPath = $newPath.$this->viewName.DS.$component_view_name.DS.$this->defaultViewFileName.'.php';
            }
            else
                $fullPath = $path.$this->viewName.DS.$this->defaultViewFileName.'.php';

            if(isset($path) && file_exists($fullPath)){
                require($fullPath);
                $viewLoaded = true;
            }
        }

        if(!$viewLoaded){
            if(isset($path) && file_exists($path.$this->defaultViewFileName.'.php')){
                require($path.$this->defaultViewFileName.'.php');
                $viewLoaded = true;
            }
        }




        $viewString = ob_get_clean();
        if($display===true){
            echo($viewString);
            return $viewString;
        }
        else{
            //$this->componentsView[$view_name] =
            if($this->rewriteCss === true)
                $viewData['css'] = $this->loadCss($path);

            $viewData['view'] = $viewString;
            return $viewData;
        }
    }

    /**
     * about -  load style.php or script.php files and return data
     * @param $path
     * @param $rewrite
     * @return bool|string
     */

    public function loadClientSideData($path, $rewrite, $fileName=false){


        $loaded = false;

        if(!$fileName){
            if($rewrite == 'css')
                $fileName = $this->defaultViewCssName;
            if($rewrite == 'js')
                $fileName = $this->defaultViewJsName;
        }

        if($this->viewName){
            $file = $path.$this->viewName.DS.$fileName.'.php';
            if(file_exists($file))
                $loaded = true;

        }

        if(!$loaded)
            $file = $path.$fileName.'.php';
        //dump($file);

        if(file_exists($file)){
            ob_start();
            @require($GLOBALS['APP']['DIR']['VIEW'].'view.cfg.php');
            require($file);
            $data = ob_get_clean();
            return $data;
        }
        else
            return false;

    }



    /**
     * @param string $val display a component view from componentView data
     */

    function displayComponent($val){
        if(isset($this->componentsView[$val]))
            echo($this->componentsView[$val]);

    }

    function display($val){
        echo($val);
    }


    /**
     * @param string $val           display data if false then display all data
     * @param bool $displayAnyway   display $val even if data not founded
     * @param string $type          set type data or static
     */

    function displayData($val=false, $displayAnyway=false, $type='data'){

        if($val){
            if($type == 'static'){
                if(isset($this->staticData[$val]))
                    echo($this->staticData[$val]);
                elseif($displayAnyway)
                    echo($val);
            }

            if($type == 'data'){
                if(isset($this->data[$val]))
                    echo($this->data[$val]);
                elseif($displayAnyway)
                    echo($val);
            }
        }
        else{
            $type=='static' ? $data = $this->staticData : $data = $this->data;
        }
    }

    function showData($val=false, $type='translated', $displayAnyway=true){

        switch($type){
            case 'translated':
                $data = $this->data;
                break;
            case 'ready':
                $data = $this->staticData;
                break;
            case 'page':
                $data = $this->staticData['data'];
                break;
        }

        if($val){
            if(isset($this->data[$val]))
                echo $this->data[$val];
            else
                $displayAnyway ? $this->display($val) : false;
        }
        else{
            dump($data);
        }

    }



}
