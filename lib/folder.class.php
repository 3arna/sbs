<?php

/** folder and files editing and scanning class */
class folder
{

    /**
     * about    - Scan directory for files or folders depends on $fileType value
     * @param string $path          must end with '\'
     * @param string $fileType      folders or files
     * @return array
     */

    static function scanFolder($path, $fileType='folders'){

        if(is_dir($path)){
            //scan dir
            $result = scandir($path);
            if(count($result)>2){
                $i=0;
                //scan for folders or files
                foreach($result as $val){
                    //remove . and .. from folder and file list
                    if($val == '.' || $val == '..')
                        continue;
                    //scan for folders
                    if($fileType == 'folders' && is_dir($path.$val))
                        $data[$i] = $val;
                    //scan for files
                    if($fileType == 'files' && is_file($path.$val))
                        $data[$i] = $val;
                    //scan for files and folders
                    if($fileType == 'all')
                        $data[$i] = $val;

                    $i++;

                }


                //if array return data
                if(is_array($data)){
                    return $data;
                }
                else
                    return false;
            }
        }
    }


    /**
     * about    - search for folder in certain directory
     *            OPTIMIZE THIS SEARCH FOLDER shit....
     * @static
     * @param $name
     * @param $path
     * @return array|string
     */

    static function searchFolder($name, $path){
        $data = array();
        $folders = self::scanFolder($path);

        foreach($folders as $val){
            if(self::isSubstring($val, $name)){
                array_push($data, $val);
            }
        }

        if(count($data)==1)
            $data = $data[0];
        if(count($data)<1)
            $data = false;
        return $data;

    }


    /**
     * about    - find substring if exists and return bool or position
     * @param $string
     * @param $substring
     * @param string $return eg. bool (return true or false) eg. pos (return position of substring)
     * @return bool
     */

    static function isSubstring($string, $substring, $getFromLastSpecialMark=']'){
        //if looking for substring from special mark position
        if($getFromLastSpecialMark){
            $pos = strripos($string, $getFromLastSpecialMark);
            $pos++;
            if(!$pos)
                $pos=0;
            $possibleSubstring = substr($string, $pos, strlen($string));
            if($possibleSubstring==$substring){
                return true;
            }
            else
                return false;
        }
        //if looking for string in substring as normal
        else{
            $pos = strpos($string, $substring);
            if($pos===0 || $pos>0)
                return true;
            else
                return false;
        }


    }

    /**
     *
     */

    static function manageFile($fileName, $filePath, $fileData, $do='w', $reformat=false){

        if (!is_dir($filePath))
            mkdir($filePath, 0755, true);

        $path = $filePath.$fileName;
        $file = fopen($path, $do);
        foreach($fileData as $key => $val){
            if($val){
                if($reformat == 'oneLine')
                    $val = preg_replace("/[\r\n]*/","",$val);
                if(false)
                    fwrite($file, '/*==============================< '.$key.' >=====================================*/');

                fwrite($file, $val);
            }
        }

        fclose($file);
    }


    static function combineFiles($files, $filePath, $fileName, $remove='<?php'){

        foreach($files as $key => $val){
            if(is_file($val)){
                if($key>0)
                    $content[$key] = str_replace($remove,"", file_get_contents($val));
                else
                    $content[$key] = file_get_contents($val);
            }
        }
        self::manageFile($fileName, $filePath, $content);
    }

    static function copyDir($what, $where){
        $dir = self::scanFolder($what, 'all');

        if(is_array($dir)){
            foreach($dir as $val){
                if(is_dir($what.$val)){
                    if(!is_dir($where.$val)){
                        mkdir($where.$val, 0755, true);
                    }
                    self::copyDir($what.$val.DS, $where.$val.DS);
                }
                if(is_file($what.$val)){
                    self::copyFile($val, $what, $where);
                }
            }
        }

            /*$data = shell_exec("cp -r $what $where");*/
        //dump($dir);
    }



    static function copyFile($fileName, $from, $to, $newName = false){

        if(substr($from, -1) != DS)
            $from = $from.DS;
        if(substr($to, -1) != DS)
            $to = $to.DS;

        if(is_dir($from)){
            if(!is_dir($to)){
                mkdir($to, 0755, true);
            }

            if(file_exists($from.$fileName)){
                if(!$newName)
                    $newName = $fileName;
                copy($from.$fileName, $to.$newName);
            }

        }
    }
}
