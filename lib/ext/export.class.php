<?php 

class export extends main{
    
    
    
    function __construct(){
        
        $this->exportSys();
        $this->exportSub();
        $this->exportMain();
        /*dump(view::$data);*/
    }
    
    //export index control file to app pub folder
    function exportSys(){
        $files = array(
            DIR_SYS.'control'.DS.'functions.php',
            $GLOBALS['APP']['DIR']['CONTROL'].'control.cfg.php',
            DIR_SYS.'control'.DS.'control.php'
        );
        
        //collect all script files
        
        $list = folder::scanFolder($GLOBALS['APP']['DIR']['SCRIPT'], 'files');
        foreach($list as $val)
            view::render(substr($val, 0, -4), 'SCRIPT', 0, 'def.script');
        
        $list = folder::scanFolder(DIR_SYS.'script'.DS, 'files');
        foreach($list as $val)
            view::render(substr($val, 0, -4), 'SCRIPT', 0, 'sys.script');
        
        
        $scripts = array_merge(view::$data['script'], view::$data['def.script'], view::$data['sys.script']);
        view::$data['script'] = $scripts;
        
        
        folder::combineFiles($files, $GLOBALS['PUB']['DIR']['APP'], 'index.php');
    }
    
    
    //export sub pages and subcomponents and collect styles, scripts, imgs to view data
    function exportSub(){
        $subPages = folder::scanFolder($GLOBALS['APP']['DIR']['SUBPAGE']);
        if(is_array($subPages)){
            foreach($subPages as $val){
                folder::copyFile('view.php', $GLOBALS['APP']['DIR']['SUBPAGE'].$val.DS.'view'.DS, $GLOBALS['PUB']['DIR']['SUB'], $val.'.php');
                view::loadViewParts($val, 'SUBPAGE', 0, array('style', 'script', 'img'));
            }
            
        }
        
        
        
        $subComp = folder::scanFolder($GLOBALS['APP']['DIR']['SUBCOMP']);
        if(is_array($subComp)){
            foreach($subComp as $val){
                folder::copyFile('view.php', $GLOBALS['APP']['DIR']['SUBCOMP'].$val.DS.'view'.DS, $GLOBALS['PUB']['DIR']['COMP'], $val.'.php');
                view::loadViewParts($val, 'SUBCOMP', 0, array('style', 'script', 'img'));
            }
            
        }
    }
    
    
    //export page with css and js
    function exportMain(){
        //main components and main pages
        //export style
        folder::manageFile('default.css', $GLOBALS['PUB']['DIR']['APP'], view::$data['style']);
        
        //export script
        folder::manageFile('default.js', $GLOBALS['PUB']['DIR']['APP'], view::$data['script']);
        
        //copy img
        foreach(view::$data['img'] as $val){
            folder::copyDir($val, $GLOBALS['PUB']['DIR']['IMG']);
        }
        
        
        //export pages
        foreach(view::$data['view'] as $key => $val){
            folder::manageFile($key.'.html', $GLOBALS['PUB']['DIR']['MAIN'], view::$data['view']);
        }
    }
}