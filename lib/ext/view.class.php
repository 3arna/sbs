<?php

class view{
    
    static public $viewName = 'view';
    static public $load = array('view', 'style', 'script', 'img');
    static public $data = array();
    
    /**
     * ABOUT
     * load view from view directory and return it [app/example/page/main/index/view/view.php]
     * 
     * @param string $type ['MAINPAGE', 'SUBPAGE', 'MAINPART', 'SUBPART']
     * @return string
     **/
    static function load($name, $type='MAINPAGE', $display=0, $load='view'){

        self::loadViewParts($name, $type, $display);
        
        /*dump(self::$data);*/
    }
    
    
    static function show($name){
        self::load($name, 'MAINCOMP', 1);
    }
    
    static function loadViewParts($name, $type, $display=0, $load='all'){
        
        if(is_string($load) && $load=='all'){
            if($load=='all'){
                foreach(self::$load as $val){
                    self::render($name, $type, $display, $val);
                }
            }
            else
                self::render($name, $type, $display, $load);
        }
        else if(is_array($load)){
            foreach($load as $val){
                self::render($name, $type, $display, $val);
            }
        }
        
    }
    
    /**
     * ABOUT
     * take path and use to render view. saves it in to self::$data['$name']['$load'] array. if $load='view' then display that element
     * 
     * @param string $name  ['index', 'comp1']
     * @param string $type  ['MAINCOMP', 'MAINPAGE', 'SUBCOMP', 'SUBPAGE']
     * @param string $load  ['view', 'style', 'script']
     * @param boolen $display [0, 1] displays only if $load='view'
     **/
    static function render($name, $type, $display=0, $load='view'){
        $path = self::createPath($name, $type, $load);
        
        /*dump(array($name, $type, $load, $path));*/
        
        if(isset($path) && file_exists($path) && $load !='img'){
            ob_start();
            @require($GLOBALS['APP']['DIR']['VIEW'].'view.cfg.php');
            if($load == 'view'){
                switch($type){
                    case 'MAINPAGE':
                        self::render('head', 'DEFCOMP', 1, $load);
                        echo('<div id="'.$name.'_page_keeper">');
                        require($path);
                        echo('</div>');
                        self::render('foot', 'DEFCOMP', 1, $load);
                        break;
                        
                    case 'DEFCOMP':
                        require($path);
                        break;
                    
                    default:
                        echo('<div id="'.$name.'_keeper">');
                        require($path);
                        echo('</div>');
                        
                }
            }
            else{
                if($load == 'def.script' && ($name == 'load' || $name == 'ready')){
                    if($name == 'load'){
    echo('
    window.onload = function (){
    ');
                        require($path);
    echo('
    };
    ');
                    }
                    if($name == 'ready'){
    echo('
    $(document).ready(function() {
    ');
                        require($path);
    echo('
    });
    ');
                    }
                    
                }
                else
                    require($path);
            }
            $view = ob_get_clean();
            
            //store view data to view::$data
            if($type=='MAINPAGE' || $load == 'style' || $load == 'script' || $load == 'def.script' || $load == 'sys.script')
                self::$data[$load][$name] = $view;
            
            if($display && $load=='view')
                echo($view);
        }
        else if($load == 'img'){
            self::$data[$load][$name] = $path;
        }
        else{
            msg(0, 'element doesnt exist'.' type:'.$type.' name:'.$name.' load:'.$load , 'load', 'view::render');
            return 0;
        }
    }
    
    /**
     * ABOUT
     * set path of the component by type and what u have to load or page and return it
     * 
     * @param string $name  ['index', 'comp1']
     * @param string $type  ['MAINCOMP', 'MAINPAGE', 'SUBCOMP', 'SUBPAGE']
     * @param string $load  ['view', 'style', 'script']
     * @return string   ['app/example/page/main/index/view/view.php']
     **/
    public function createPath($name, $type, $load='view'){
        
        $typeDir = $GLOBALS['APP']['DIR'][$type];
        $path = false;
        switch($load){
            case 'view':
                $dir = $load;
                $path = $typeDir.$name.DS.$dir.DS.$load.'.php';
                break;
                
            case 'style':
                $dir = 'view';
                $path = $typeDir.$name.DS.$dir.DS.$load.'.php';
                break;
                
            case 'script':
                $dir = $load;
                $path = $typeDir.$name.DS.$dir.DS.$load.'.php';
                break;
                
            case 'img':
                $dir = $load;
                $path = $typeDir.$name.DS.'view'.DS.$dir.DS;
                break;
                
            case 'def.script':
                $path = $GLOBALS['APP']['DIR']['SCRIPT'].$name.'.php';
                break;
                
            case 'sys.script':
                $path = DIR_SYS.'script'.DS.$name.'.php';
                break;
        }
        
        return $path;
    }
    
    
    
    

    public function p($component_view_name, $display=false, $data=false){

        $isDefault = false;

        if($display===true || $display==='page'){
            $folderName = folder::searchFolder($component_view_name, $GLOBALS['APP']['DIR']['PAGE']);
            if($folderName)
                $path = $GLOBALS['APP']['DIR']['PAGE'].$folderName.DS.$this->defaultViewFolder.DS;
        }
        else{
            if(folder::isSubstring($component_view_name, '~', false)){
                $isDefault = true;
                $path = $GLOBALS['APP']['DIR']['VIEW'].$this->defaultViewFolder.DS.$component_view_name.DS;
            }
            else{
                $folderName = folder::searchFolder($component_view_name, $GLOBALS['APP']['DIR']['COMPONENT']);
                if($folderName)
                    $path = $GLOBALS['APP']['DIR']['COMPONENT'].$folderName.DS.$this->defaultViewFolder.DS;
            }
        }

        $app = $GLOBALS['APP'];

        ob_start();

        //extract data if is set
        @require($GLOBALS['APP']['DIR']['VIEW'].'view.cfg.php');
        if($app)
            extract($app, EXTR_SKIP);

        $viewLoaded = false;

        if($this->viewName){

            if($isDefault){
                $newPath = substr($path, 0, strpos($path, $component_view_name));
                $fullPath = $newPath.$this->viewName.DS.$component_view_name.DS.$this->defaultViewFileName.'.php';
            }
            else
                $fullPath = $path.$this->viewName.DS.$this->defaultViewFileName.'.php';

            if(isset($path) && file_exists($fullPath)){
                require($fullPath);
                $viewLoaded = true;
            }
        }

        if(!$viewLoaded){
            if(isset($path) && file_exists($path.$this->defaultViewFileName.'.php')){
                require($path.$this->defaultViewFileName.'.php');
                $viewLoaded = true;
            }
        }




        $viewString = ob_get_clean();
        if($display===true){
            echo($viewString);
            return $viewString;
        }
        else{
            //$this->componentsView[$view_name] =
            if($this->rewriteCss === true)
                $viewData['css'] = $this->loadCss($path);

            $viewData['view'] = $viewString;
            return $viewData;
        }
    }

    /**
     * about -  load style.php or script.php files and return data
     * @param $path
     * @param $rewrite
     * @return bool|string
     */

    public function loadClientSideData($path, $rewrite, $fileName=false){


        $loaded = false;

        if(!$fileName){
            if($rewrite == 'css')
                $fileName = $this->defaultViewCssName;
            if($rewrite == 'js')
                $fileName = $this->defaultViewJsName;
        }

        if($this->viewName){
            $file = $path.$this->viewName.DS.$fileName.'.php';
            if(file_exists($file))
                $loaded = true;

        }

        if(!$loaded)
            $file = $path.$fileName.'.php';
        //dump($file);

        if(file_exists($file)){
            ob_start();
            @require($GLOBALS['APP']['DIR']['VIEW'].'view.cfg.php');
            require($file);
            $data = ob_get_clean();
            return $data;
        }
        else
            return false;

    }



    /**
     * @param string $val display a component view from componentView data
     */

    function displayComponent($val){
        if(isset($this->componentsView[$val]))
            echo($this->componentsView[$val]);

    }

    function display($val){
        echo($val);
    }


    /**
     * @param string $val           display data if false then display all data
     * @param bool $displayAnyway   display $val even if data not founded
     * @param string $type          set type data or static
     */

    function displayData($val=false, $displayAnyway=false, $type='data'){

        if($val){
            if($type == 'static'){
                if(isset($this->staticData[$val]))
                    echo($this->staticData[$val]);
                elseif($displayAnyway)
                    echo($val);
            }

            if($type == 'data'){
                if(isset($this->data[$val]))
                    echo($this->data[$val]);
                elseif($displayAnyway)
                    echo($val);
            }
        }
        else{
            $type=='static' ? $data = $this->staticData : $data = $this->data;
        }
    }

    function showData($val=false, $type='translated', $displayAnyway=true){

        switch($type){
            case 'translated':
                $data = $this->data;
                break;
            case 'ready':
                $data = $this->staticData;
                break;
            case 'page':
                $data = $this->staticData['data'];
                break;
        }

        if($val){
            if(isset($this->data[$val]))
                echo $this->data[$val];
            else
                $displayAnyway ? $this->display($val) : false;
        }
        else{
            dump($data);
        }

    }



}
