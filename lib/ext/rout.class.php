<?php 

class rout extends main{
    
    public $app = 'example';
    
    
    function __construct(){
       $this->app = $this->checkApp();
    }
    
    
    /**
     * ABOUT
     * find app name from $_GET['url'] and return it if exists in app directory
     * 
     * @return string   ['example']
     **/
    function checkApp(){
        
        if(isset($_GET['url']))
            $url = string::strExplode('/', $_GET['url']);
            
        $app = folder::searchFolder($url[0], DIR_APP);
        if($app){
            $this->msg(1, 'app found and set succesfully', __METHOD__);
            return $app;
        }
        else{
            return false;
            $this->msg(0, 'incorrect application name', __METHOD__);
        }
    }
}