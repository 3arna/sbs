<?php

    class main{
        
        private $name;
    
        function __construct(){
            
            $this->name = get_called_class();
        }
        
        function msg($done, $msg, $method='unknown'){
            $this->name = get_called_class();
            msg($done, $msg, $this->name, $method);
        }
        
        function req($url, $return=1){
            
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_CONNECTTIMEOUT => 10,
                CURLOPT_TIMEOUT => 20
            ));
            
            $data = curl_exec($curl);
            curl_close($curl);
            
            if($return){
                return json_decode($data, true);
            } 
            else
                print $data;
        }
        
    }