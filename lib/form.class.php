<?php

/** form serialisation and validation class */
class form{


    /**
     * about    - validate form values
     * @param array $array
     * @param string $return        :bool returns true or false
     *                              :valid_array returns valid array or false
     *                              :clean_array returns array where invalid values will be replaced as false
     * @param bool $hash_password
     * @return mixed
     */
    public $types = array(
        'email',
        'number',
        'password',
        'name',
        'serial',
        'url',
        'record',
        'date',
        'pack'
    );

    public function validate($array, $return='bool', $hash_password=false){

        //its from security class input filter types
        $types = $this->types;


        foreach($array as $key => $val){
            $type = 'none';
            //check type of the array element by his name
            for($i=0; $i<count($types); $i++){
                $pos = stripos($key, $types[$i]);
                if($pos!== false){
                    $type = $types[$i];
                    if($type=='record' || $type=='pack'){
                        $newkey = strtolower(str_replace($type, '', $key));
                        $array[$newkey] = $array[$key];
                        unset($array[$key]);
                        $key = $newkey;
                    }
                    break;
                }
            }

            $valid = false;
            if(!is_array($array[$key]))
                $array[$key] = security::inputFilter($val, $type);
            if(is_string($array[$key]) || is_array($array[$key])){
                switch($type){

                    case 'email':
                    $valid = true;
                    break;

                    case 'pack':
                    $valid = true;
                    break;

                    case 'number':
                        $valid = true;
                    break;

                    case 'record':
                        $valid = true;
                    break;

                    case 'date':
                        $valid=true;
                    break;

                    case 'password':
                        if($type == 'password' && $hash_password){
                            $array[$key] = security::hashString($array[$key], 'password');
                            $valid = true;
                        }
                    break;

                    case 'serial':
                        if($array['serial']==session::get('formSerial'))
                            $valid = true;
                    break;

                    case 'url':
                        $valid = true;
                    break;
                }
            }

            if($valid===false)
                break;

            /*if($valid===false)
                $invalid[$key] = $type;*/

        }
        //dump($invalid);
        //return false or array

        switch($return){

            case 'bool':
                return $valid;
            break;

            case 'valid_array':
                if($valid){
                    //delete serial from array
                    unset($array['serial']);
                    return $array;
                }
                else
                    return false;
            break;

            case 'clean_array':
                if(!$valid)
                    return $array;
            break;
        }

    }

    public function removeTypes($post){
        foreach($post as $key => $val){
            //var_dump($key);
        }
    }

}
